import numpy as np

def split_data(X, y, tr_fraction=0.5):
    """
    Split the data X,y into two random subsets

    """
    num_samples = y.size
    n_tr = int(num_samples * tr_fraction)

    idx = np.array(range(num_samples))
    np.random.shuffle(idx)  # shuffle the elements of idx

    tr_idx = idx[0:n_tr]
    ts_idx = idx[n_tr:]

    Xtr = X[tr_idx, :]
    ytr = y[tr_idx]

    Xts = X[ts_idx, :]
    yts = y[ts_idx]

    return Xtr, ytr, Xts, yts

def count_digits(y):
    """
    Count the number of elements in each class

    Parameters
    ----------
    y : ndarray
        the labels of each sample.

    Returns
    -------
    v : ndarray
        the number of elements in each class.
    """
    num_classes = np.unique(y).size  # number of unique elements in y
    classes = np.unique(y)
    p = np.zeros(shape=(num_classes,), dtype=int)

    for k in xrange(num_classes):
        p[k] = np.sum(y == classes[k])

    return p

def fit(Xtr, ytr):
    """Compute the average centroid for each class"""
    num_classes = np.unique(ytr).size
    centroids = np.zeros(shape=(num_classes, Xtr.shape[1]))
    for k in xrange(num_classes):
        xk = Xtr[ytr == k, :]
        centroids[k, :] = np.mean(xk, axis=0)
    return centroids